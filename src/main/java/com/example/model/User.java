package com.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name="user")
public class User {
	
	@Id
	@GeneratedValue
	private Long Id;
	
	@Column(unique=true)
	private String mail;
	private String facebookToken;
	
	public User(String mail) {
		super();
		this.mail = mail;
	}
	
	public User() {
	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getFacebookToken() {
		return facebookToken;
	}

	public void setFacebookToken(String facebookToken) {
		this.facebookToken = facebookToken;
	}

	@Override
	public String toString() {
		return "User [Id=" + Id + ", mail=" + mail + ", facebookToken=" + facebookToken + "]";
	}
	
}
