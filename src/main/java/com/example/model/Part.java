package com.example.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonView;

@Entity(name="part")
public class Part {

	@Id
	@GeneratedValue
	@JsonView(ViewPart.Summary.class)
	private Long id;
	@JsonView(ViewPart.Summary.class)
	private String name;
	@JsonView(ViewPart.Summary.class)
	private String description;
	@JsonView(ViewPart.Summary.class)
	private int highScore;
	@JsonView(ViewPart.Summary.class)
	private String icon;

	@OneToMany(mappedBy="part")
	private List<Sequence> sequences;
	
	public Part() {
	}
	
	public String getIcon() {
		return icon;
	}
	
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	public int getHighScore() {
		return highScore;
	}
	
	public void setHighScore(int highScore) {
		this.highScore = highScore;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Sequence> getSequences() {
		return sequences;
	}

	public void setSequences(List<Sequence> sequences) {
		this.sequences = sequences;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Part [id=" + id + ", name=" + name + ", sequences=" + sequences + "]";
	}
	
}
