package com.example.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import com.fasterxml.jackson.annotation.JsonView;

@Entity(name="lesson")
public class Lesson {
	
	@Id
	@GeneratedValue
	@JsonView(ViewLesson.Summary.class)
	private Long id;
	
	@JsonView(ViewLesson.Summary.class)
	private String title;

	@JsonView(ViewLesson.Summary.class)
	private String lesson;
	
	@JsonView(ViewLesson.Summary.class)
	private ArrayList<String> examples;

	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public String getLesson() {
		return lesson;
	}



	public void setLesson(String lesson) {
		this.lesson = lesson;
	}



	public ArrayList<String> getExamples() {
		return examples;
	}



	public void setExamples(ArrayList<String> examples) {
		this.examples = examples;
	}



	@Override
	public String toString() {
		return this.id+this.title+this.lesson;
	}

}
