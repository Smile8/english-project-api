package com.example.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.Part;
import com.example.model.Sequence;

public interface SequenceRepository extends JpaRepository<Sequence, Long> {
	
	List<Sequence> findByPart(Part part);
	
}