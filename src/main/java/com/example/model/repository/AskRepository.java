package com.example.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.Ask;

public interface AskRepository extends JpaRepository<Ask, Long> {	
}