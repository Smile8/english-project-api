package com.example.model;

import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonView;

@Entity(name="ask")
public class Ask {

	@Id
	@GeneratedValue
	@JsonView(View.Summary.class)
	private Long id;
	
	@JsonView(View.Summary.class)
	private String title;

	@JsonView(View.Summary.class)
	private String response;
	
	@JsonView(View.Summary.class)
	private ArrayList<String> answers;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="sequence_id")
	private Sequence sequence;
	
	public Ask(String title) {
		super();
		this.title = title;
	}
	
	public Ask() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public ArrayList<String> getAnswers() {
		return answers;
	}

	public void setAnswers(ArrayList<String> answers) {
		this.answers = answers;
	}

	public Sequence getSequence() {
		return sequence;
	}

	public void setSequence(Sequence sequence) {
		this.sequence = sequence;
	}

	@Override
	public String toString() {
		return "Ask [id=" + id + ", title=" + title + ", response=" + response + ", answers=" + answers + "]";
	}
	
}
