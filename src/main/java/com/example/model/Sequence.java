package com.example.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonView;

@Entity(name="sequence")
public class Sequence {
	
	@Id
	@GeneratedValue
	@JsonView(View.Summary.class)
	private Long id;
	
	@OneToMany(mappedBy="sequence",fetch=FetchType.EAGER, cascade = CascadeType.ALL)
	@JsonView(View.Summary.class)
	private List<Ask> asks;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="part_id")
	private Part part;
	
	@JsonView(View.Summary.class)
	private ArrayList<String> textes;
	@JsonView(View.Summary.class)
	private String soundUrl;
	@JsonView(View.Summary.class)
	private String imageUrl;
	
	public Sequence() {
	}

	public Sequence(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Ask> getAsks() {
		return asks;
	}

	public void setAsks(List<Ask> asks) {
		this.asks = asks;
	}

	public Part getPart() {
		return part;
	}

	public void setPart(Part part) {
		this.part = part;
	}

	public ArrayList<String> getTextes() {
		return textes;
	}

	public void setTextes(ArrayList<String> textes) {
		this.textes = textes;
	}

	public String getSoundUrl() {
		return soundUrl;
	}

	public void setSoundUrl(String soundUrl) {
		this.soundUrl = soundUrl;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	@Override
	public String toString() {
		return "Sequence [id=" + id + ", asks=" + asks + ", textes=" + textes + ", soundUrl="
				+ soundUrl + ", imageUrl=" + imageUrl + "]";
	}
	
}
