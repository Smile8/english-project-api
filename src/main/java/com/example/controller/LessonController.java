package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.controller.exception.LessonNotFoundException;
import com.example.model.Lesson;
import com.example.model.ViewLesson;
import com.example.model.repository.LessonRepository;
import com.fasterxml.jackson.annotation.JsonView;

@RestController
public class LessonController extends BaseController {
	
	@Autowired
	private LessonRepository lessonRepository;
	
	@RequestMapping(path="/lessons", method=RequestMethod.GET)
	@JsonView(ViewLesson.Summary.class)
	public List<Lesson> getLessons () {
		return this.lessonRepository.findAll();
	}
	
	@ExceptionHandler(LessonNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public String handleLessonNotFoundException(LessonNotFoundException e) {
		return e.getMessage();
	}
	
}
