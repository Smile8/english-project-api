package com.example.controller;

import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Ask;
import com.example.model.Lesson;
import com.example.model.Sequence;
import com.example.model.repository.AskRepository;
import com.example.model.repository.LessonRepository;

@RestController
public class InitController extends BaseController {
	
	@Autowired
	private AskRepository askRepository;
	
	@Autowired
	private LessonRepository lessonRepository;
	
	@RequestMapping(path="/init/asks", method=RequestMethod.GET)
	public void upAsk() {

		ArrayList<String> answers = new ArrayList<String>();
		answers.add("A");
		answers.add("B");
		answers.add("C");
		//answers.add("D");
		
		//this.askRepository.deleteAll();
		
		Ask ask = new Ask();
		ask.setResponse("B");
		ask.setTitle("");
		ask.setAnswers(answers);
		Sequence sequence = new Sequence((long) 1);
		ask.setSequence(sequence);
		this.askRepository.save(ask);

		/*ask = new Ask();
		ask.setResponse("B");
		ask.setTitle("");
		ask.setAnswers(answers);
		sequence = new Sequence((long) 2);
		ask.setSequence(sequence);
		ask.setResponse("A");
		this.askRepository.save(ask);*/
	}
	
	@RequestMapping(path="/init/lessons", method=RequestMethod.GET)
	public void upLesson() {
		
		this.lessonRepository.deleteAll();
		
		ArrayList<String> examples = new ArrayList<String>();
		examples.add("example A");
		examples.add("example B");
		examples.add("example C");
		examples.add("example D");
		
		Lesson lesson = new Lesson();
		lesson.setId((long) 1);
		lesson.setTitle("Lesson 1 : Basics");
		lesson.setLesson("this is the lesson's content... And ok maybe, it's a little bit empty");
		lesson.setExamples(examples);
		this.lessonRepository.save(lesson);
		
		lesson.setId((long) 2);
		lesson.setTitle("Lesson 2 : Advanced");
		lesson.setLesson("this is the lesson's content... And ok maybe, it's a little fast to already go to the advanced section");
		lesson.setExamples(examples);
		this.lessonRepository.save(lesson);

	}

}
