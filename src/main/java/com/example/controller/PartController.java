package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.controller.exception.PartNotFoundException;
import com.example.model.Part;
import com.example.model.ViewPart;
import com.example.model.repository.PartRepository;
import com.fasterxml.jackson.annotation.JsonView;

@RestController
public class PartController extends BaseController {
	
	@Autowired
	private PartRepository partRepository;
	
	@RequestMapping(path="/parts", method=RequestMethod.GET)
	@JsonView(ViewPart.Summary.class)
	public List<Part> getParts () {
		return this.partRepository.findAll();
	}
	
	@ExceptionHandler(PartNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public String handlePartNotFoundException(PartNotFoundException e) {
		return e.getMessage();
	}
	
}
