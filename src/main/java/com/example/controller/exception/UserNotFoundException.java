package com.example.controller.exception;

public class UserNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public UserNotFoundException(Long userId) {
		super("User " + userId + " not found !");
	}

}
