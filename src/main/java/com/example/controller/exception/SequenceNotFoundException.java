package com.example.controller.exception;

public class SequenceNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public SequenceNotFoundException(Long seqId) {
		super("Sequence " + seqId + " not found !");
	}
}
