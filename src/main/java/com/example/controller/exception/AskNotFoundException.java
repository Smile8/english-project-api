package com.example.controller.exception;

public class AskNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public AskNotFoundException(Long askId) {
		super("Ask " + askId + " not found !");
	}
}