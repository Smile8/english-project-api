package com.example.controller.exception;

public class LessonNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public LessonNotFoundException(Long lessonId) {
		super("Lesson " + lessonId + " not found !");
	}
}
