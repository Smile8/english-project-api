package com.example.controller.exception;

public class PartNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public PartNotFoundException(Long partId) {
		super("Part " + partId + " not found !");
	}

}
