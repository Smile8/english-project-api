package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.controller.exception.PartNotFoundException;
import com.example.controller.exception.SequenceNotFoundException;
import com.example.model.Part;
import com.example.model.Sequence;
import com.example.model.View;
import com.example.model.repository.SequenceRepository;
import com.fasterxml.jackson.annotation.JsonView;

@RestController
public class SequenceController extends BaseController{
	
	@Autowired
	private SequenceRepository sequenceRepository;
	
	@RequestMapping(path="/sequences", method=RequestMethod.GET)
	public List<Sequence> getSequences () {
		return this.sequenceRepository.findAll();
	}
	
	@ExceptionHandler(PartNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public String handleSequenceNotFoundException(SequenceNotFoundException e) {
		return e.getMessage();
	}
	
	@RequestMapping(path="/parts/{partsId}/sequences", method=RequestMethod.GET)
	@JsonView(View.Summary.class)
	public List<Sequence> getRandomSequences (@PathVariable Long partsId) {
		Part part = new Part();
		part.setId(partsId);
		List<Sequence> sequences = this.sequenceRepository.findByPart(part);
		return sequences;
	}

}
